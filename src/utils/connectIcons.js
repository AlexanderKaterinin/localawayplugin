import Vue from 'vue';
import LiveLabel from '../components/icons/LiveLabel.vue';
import LogoBlack from '../components/icons/LogoBlack.vue';
import LogoWhite from '../components/icons/LogoWhite.vue';
import ExpandIcon from '../components/icons/ExpandIcon.vue';
import PauseIcon from '../components/icons/PauseIcon.vue';
import MinimizeIcon from '../components/icons/MinimizeIcon.vue';
import SendIcon from '../components/icons/SendIcon.vue';

export default {
    connect () {
        Vue.component('live-label', LiveLabel);
        Vue.component('logo-black', LogoBlack);
        Vue.component('logo-white', LogoWhite);
        Vue.component('expand-icon', ExpandIcon);
        Vue.component('pause-icon', PauseIcon);
        Vue.component('minimize-icon', MinimizeIcon);
        Vue.component('send-icon', SendIcon);
    }
};
