import TOAST_TYPES from '../../contants/TOAST_TYPES';

export default class ToastService {
    #store;

    constructor(store) {
        this.#store = store;
    }

    #show (text, type) {
        this.#store.commit('showToast', { text, type });
    }

    success(text) {
        this.#show(text, TOAST_TYPES.SUCCESS);
    }

    error(text) {
        this.#show(text, TOAST_TYPES.ERROR);
    }

    warning(text) {
        this.#show(text, TOAST_TYPES.WARNING);
    }

    info(text) {
        this.#show(text, TOAST_TYPES.INFO);
    }

}
