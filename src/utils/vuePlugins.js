import ToastService from './Service/ToastService';
import store from '../store/store';
export default {
    install (Vue, options) {
        Vue.mixin({
            computed: {
                $toast () {
                    return new ToastService(store);
                }
            }
        })
    }
}
