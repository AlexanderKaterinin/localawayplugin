import moment from 'moment-timezone';

/*
 |
 | Based on https://www.npmjs.com/package/add2calendar
 |
 */
const CalendarsApi = {

    makeGoogleUrl (event) {
        event = this.eventNormalize(event);
        const startDate = this.googleTimeFormat(event.start, event);
        const endDate = this.googleTimeFormat(event.end, event);
        const args = {
            text: event.title,
            dates: startDate + '/' + endDate,
            ctz: event.timezone,
            details: event.description,
            location: event.url,
        };

        return 'https://calendar.google.com/calendar/u/0/r/eventedit?' + this.mapToQuery(args);
    },

    makeOutlookOnlineUrl (event) {
        event = this.eventNormalize(event);
        const startdt = this.outlookOnlineTimeFormat(event.start, event);
        const enddt = this.outlookOnlineTimeFormat(event.end, event);

        const args = {
            path: '/calendar/action/compose',
            rru: 'addevent',
            //
            startdt,
            enddt,
            subject: event.title,
            location: event.url,
            body: event.description,
        };

        return 'https://outlook.live.com/calendar/0/deeplink/compose?' + this.mapToQuery(args);
    },

    makeYahooUrl (event) {
        event = this.eventNormalize(event);
        const ST = this.yahooTimeFormat(event.start, event);
        const ET = this.yahooTimeFormat(event.end, event);

        const args = {
            v: 60,
            view: 'd',
            type: 20,
            //
            TITLE: event.title,
            ST,
            ET,
            DESC: event.description,
            in_loc: event.url,
        };

        return 'https://calendar.yahoo.com/?' + this.mapToQuery(args);
    },

    makeICalIcsUrl (event) {
        event = this.eventNormalize(event);

        return encodeURI(
            'data:text/calendar;charset=utf8,' +
            [
                'BEGIN:VCALENDAR',
                'VERSION:2.0',
                'BEGIN:VTIMEZONE',
                'TZID:' + event.timezone,
                'END:VTIMEZONE',
                'BEGIN:VEVENT',
                'URL:' + event.url,
                'DTSTART:' + this.icsTimeFormat(event.start, event),
                'DTEND:' + this.icsTimeFormat(event.end, event),
                'SUMMARY:' + event.title,
                'DESCRIPTION:' + event.description,
                'LOCATION:' + event.url,
                'END:VEVENT',
                'END:VCALENDAR',
            ].join('\n')
        );
    },

    makeOutlookIcsUrl (event) {
        return this.makeICalIcsUrl(event);
    },

    googleTimeFormat (time, event) {
        return moment(time + ' +00:00', event.timeFormat + ' Z').tz(event.timezone).format('YYYYMMDD[T]HHmmss');
    },

    outlookOnlineTimeFormat (time, event) {
        const tzNumeric = moment().tz(event.timezone).format('Z');
        time = time + ' ' + tzNumeric;
        const timeFormat = event.timeFormat + ' Z';
        return moment(time, timeFormat).tz('UTC').format('YYYY-MM-DD[T]HH:mm:ss[Z]');
    },

    yahooTimeFormat (time, event) {
        return moment(time + ' +00:00', event.timeFormat + ' Z').tz(event.timezone).format('YYYYMMDD[T]HHmmss');
    },

    icsTimeFormat (time, event) {
        return moment(time + ' +00:00', event.timeFormat + ' Z').tz(event.timezone).format('YYYYMMDD[T]HHmmss');
    },

    mapToQuery (map) {
        const str = [];
        for (const k in map) {
            if (Object.prototype.hasOwnProperty.call(map, k)) {
                str.push(encodeURIComponent(k) + '=' + encodeURIComponent(map[k]));
            }
        }

        return str.join('&');
    },

    eventNormalize (event) {
        for (const key of ['start', 'end', 'title', 'timeFormat', 'timezone']) {
            if (!event[key]) {
                throw new Error(`Property event.${key} is required`);
            }
        }

        return Object.assign({
            description: '',
            url: '',
        }, event);
    },
};

export { CalendarsApi };
