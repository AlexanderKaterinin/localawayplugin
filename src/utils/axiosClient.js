import axios from 'axios';
import store from './../store/store';

const axiosClient = axios.create();

axiosClient.interceptors.request.use((config) => {
    config.baseURL = store.state.core.API_HOST + '/';
    return config;
})

export default axiosClient;
