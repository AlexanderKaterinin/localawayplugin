import Pusher from 'pusher-js';

export default {
    broadcaster: 'pusher',
    key: import.meta.env.VITE_ECHO_APP_KEY || 'efd1c16f956531c5e73f',
    cluster: import.meta.env.VITE_ECHO_CLUSTER || 'mt1',
    forceTLS: true
}
