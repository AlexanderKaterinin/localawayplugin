export function __hardcoded (value) {
    return value;
}
export function undefFallback (value, fallback) {
    return (value !== undefined) ? value : fallback;
}

export const capitalizeFirstLetter = (str = '') => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
