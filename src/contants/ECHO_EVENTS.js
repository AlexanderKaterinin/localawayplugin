export default {
    MESSAGE_SENT: '.message.sent',
    MESSAGE_ADDED: '.message.added',
    MESSAGE_DELETED: '.message.deleted',
    STREAM_FINISHED: '.stream.finished',
    FEATURED_LOCATION_MESSAGE_ADDED: '.message.added.location.',
    FEATURED_LOCATION_MESSAGE_DELETED: '.message.deleted.location.',
}
