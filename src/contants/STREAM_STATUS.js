export default {
    PLANNED: 'planned',
    ACTIVE: 'active',
    COMPLETED: 'completed',
    FINISHED: 'finished'
}
