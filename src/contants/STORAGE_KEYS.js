export default {
    MODAL_TOP_POSITION: 'lclwy_modal_top_position',
    MODAL_LEFT_POSITION: 'lclwy_modal_left_position',
    ACTIVE_EVENT_ID: 'lclwy_active_event_id',
    ACTIVE_STYLIST_ID: 'lclwy_active_stylist_id',
    ACTIVE_EVENT_TYPE: 'lclwy_active_event_type',
    CHAT_NAME: 'lclwy_chat_login',
    IS_STREAM_MUTED: 'lclwy_is_muted'
}
