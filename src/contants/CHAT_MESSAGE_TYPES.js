export default {
    TYPE_DEFAULT: 'default',
    TYPE_USER_JOINED: 'joined',
    TYPE_ON_SALE: 'on_sale',
    TYPE_SOLD_OUT: 'sold_out',
    TYPE_LOCALAWAY: 'localaway',
    TYPE_DIRECT: 'direct'
}
