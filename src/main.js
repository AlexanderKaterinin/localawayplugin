import Vue from 'vue';
import App from './App.vue';
import './index.css';
import store from './store/store';
import LDiv from './components/utils/LDiv.vue';
import LoadingSpinner from './components/utils/LoadingSpinner.vue';
import VueEcho from 'vue-echo-laravel/vue-echo';
import vueEchoConfig from './utils/vueEchoConfig';
import connectIcons from './utils/connectIcons';
import VueSocialSharing from 'vue-social-sharing'
import vuePlugins from './utils/vuePlugins';

Vue.component('l-div', LDiv);
Vue.component('loading', LoadingSpinner);
Vue.use(VueEcho, vueEchoConfig);
Vue.use(VueSocialSharing);
Vue.use(vuePlugins);
connectIcons.connect();

let rootElement = '#localaway';
let API_KEY = import.meta.env.VITE_DEFAULT_API_KEY;
let API_HOST = import.meta.env.VITE_DEFAULT_API_HOST;

if (import.meta.env.PROD) {
    rootElement = document.createElement('l-div');
    rootElement.id = 'localaway';
    document.body.append(rootElement);
    const urlParams = new URL(document.currentScript.getAttribute('src'));
    const scriptParams = Object.fromEntries(urlParams.searchParams);
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    if (!scriptParams.API_KEY) {
        throw new Error("No specified API KEY!")
    } else {
        API_KEY = scriptParams.API_KEY;
        API_HOST = urlParams.origin;
    }
    store.commit('SET_DEFAULTS', {API_KEY, API_HOST})

    new Vue({
        render: (h) => h(App),
        store
    }).$mount(rootElement);
} else {
    store.commit('SET_DEFAULTS', {API_KEY, API_HOST})

    new Vue({
        render: (h) => h(App),
        store
    }).$mount(rootElement);
}
