import ECHO_EVENTS from '../contants/ECHO_EVENTS';
import {mapActions} from 'vuex';

export default {
    data: () => ({
        echoChatBroadcastChannelName: 'localaway-development',
        chatChannelName: undefined
    }),
    computed: {
        event() {
            return this.$store.getters.ACTIVE_EVENT;
        }
    },
    methods: {
        ...mapActions({
            '$getMessages': 'getMessages'
        }),
        async _initChat(channelName = undefined) {
            if (!channelName) {
                throw new Error('No channel name specified!');
            }
            this.chatChannelName = channelName;
            this.$echo
                .channel(this.echoChatBroadcastChannelName)
                .listen('.message.added', (response) => {
                    (response.channel === this.chatChannelName) && this.$store.commit('addMessage', response);
                })
                .listen('.message.sent', (response) => {
                    (response.channel === this.chatChannelName) && this.$store.commit('addMessage', response);
                })
                .listen('.stream.finished', (response) => {
                    if (this.event.id === response.id) {
                        if (this.event.status !== 'finished') {
                            this.$store.commit('SET_STREAM_FINISHED', this.event);
                            //this.$toast.success('Thanks for joining the livestream, see you next time!');
                        }
                    }
                })
                .listen(ECHO_EVENTS.MESSAGE_DELETED, (res) => {
                    this.$store.commit('deleteMessage', res.id);
                });

            try {
                await this.$getMessages(channelName);
            } catch (e) {
                this.error = true;
            }
        },
        async _destroyChat() {
            this.$echo.channel(this.echoChatBroadcastChannelName)
                .stopListening(ECHO_EVENTS.MESSAGE_ADDED)
                .stopListening(ECHO_EVENTS.MESSAGE_DELETED)
        }
    }
}
