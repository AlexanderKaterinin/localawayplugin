import AgoraStreamManager from '../utils/AgoraStreamManager';
import AgoraRTC from 'agora-rtc-sdk-ng';
import STREAM_STATUS from '../contants/STREAM_STATUS';
import axiosClient from '../utils/axiosClient';
import {undefFallback} from '../utils/helpers';
import STORAGE_KEYS from '../contants/STORAGE_KEYS';

/**
 * @typedef {('host', 'co_host', 'subscriber')}
 * @name StreamParticipantRole
 */
/**
 * @typedef {('planned', 'active', 'finished', 'completed')}
 * @name StreamStatus
 */
/**
 * @typedef {{
 *   id: Number,
 *   user: { location: {mixed}, },
 *   channel: string,
 *   status: StreamStatus,
 *   views_count: ?int,
 *   chat_messages_count: ?int,
 *   likes_count: ?int,
 * }}
 * @name Stream
 */

const useStreamClientMixin = {
    data: () => ({
        /*
         | stream-related
         */
        /**
         * @type {Stream}
         */
        stream: undefined,
        /** @type {AgoraStreamManager} */
        streamManager: undefined,
        participants: {},
        streamChannelName: undefined,
        firstHostWithPublishedAudio: undefined,
        isStreamLive: false,
        /*
         | stats
         */
        /*
         | flags & state enums
         */
        haveIJoined: false,
        // in real time: when host enters channel, this flag is set to TRUE (for all participants instances)
        hasStreamBeenStartedRecently: false,
        // in real time: when host presses "End Event" button, this flag is set to TRUE (for all participants instances)
        hasStreamBeenFinishedRecently: false,
        /**
         * @see on(`user-unpublish`) below
         * 0 - default, 1 - start to unpublish, 2 - unpublish complete
         */
        hostUnpublishState: 0,
        echoBroadcastChannelName: 'localaway-development',
    }),
    computed: {
        client () {
            return this.streamManager.client();
        },
        roleVerbose () {
            return this.streamManager?.getRoleVerbose();
        },
        isStreamActive () {
            return this.doesStreamHasStatus(STREAM_STATUS.ACTIVE);
        },
        isStreamInteractive () {
            return this.doesStreamHasStatus([STREAM_STATUS.ACTIVE, STREAM_STATUS.COMPLETED]);
        },
        isStreamPlanned () {
            return this.doesStreamHasStatus(STREAM_STATUS.PLANNED);
        },
        isStreamOpen () {
            return !this.isStreamClosed;
        },
        isStreamClosed () {
            return this.doesStreamHasStatus([STREAM_STATUS.COMPLETED, STREAM_STATUS.FINISHED]);
        },
        isStreamFinished () {
            return this.doesStreamHasStatus(STREAM_STATUS.FINISHED);
        },
        streamCouldBeJoinedBy () {
            return this.isStreamOpen;
        },
        amIHost () {
            return this.roleVerbose === 'host';
        },
        amICoHost () {
            return this.roleVerbose === 'co_host';
        },
        amIAudience () {
            return this.roleVerbose === 'subscriber';
        },
        /**
         * Can I start the stream?
         * @return {boolean}
         */
        amIPublisher () {
            return this.amIHost || this.amICoHost;
        },
        myRtcUid () {
            return this.streamManager.getUid();
        },
        coHosts () {
            return this.filterParticipants(participant => participant.role === 'co_host');
        },
        hostUid (uid) {
            for (uid in this.participants) {
                if (this.isHost(uid)) {
                    return Number.parseInt(uid);
                }
            }
            return undefined;
        },
        /**
         * @return {StreamStatus|undefined}
         */
        streamStatusReactive () {
            if (this.hasStreamBeenFinishedRecently) { return 'completed'; }
            if (this.hasStreamBeenStartedRecently) { return 'active'; }
            return this.stream ? this.stream.status : undefined;
        },
    },
    methods: {
        /*
         | Init methods
         */
        /**
         * !this.stream should be initialized before
         * @param options
         * @return {Promise<void>}
         * @private
         */
        async _initStream (options) {
            this._initParticipantsFromStreamData();
            this.visitorsCount = undefFallback(this.stream?.views_count, 0);
            this.chatMessagesCount = undefFallback(this.stream?.chat_messages_count, 0);
            this.likesCount = undefFallback(this.stream?.likes_count, 0);
            this.streamManager = new AgoraStreamManager({
                app_id: this.$store.getters.AGORA_APP_ID,
                channel_name: this.streamChannelName,
                rtc_token: options.token,
                role_verbose: options.role_verbose,
                uid: options.uid,
            });
            await this.streamManager.initClient();
            this._subscribeToRtcEvents();
        },
        _initViewers () {
            this.$echo.channel(this.echoBroadcastChannelName)
                .listen('.stream.stats', (res) => {
                    if (res.channel === this.streamChannelName) {
                        this.$store.commit('SET_STREAM_STATS', res);
                    }
                })
        },
        _initParticipantsFromStreamData () {
            this._addPublishers(
                this.stream.user_id,
                (this.stream.participants || []).map(({ id }) => id),
            );
        },
        _subscribeToRtcEvents () {
            this.client.on('user-published', async (user, mediaType) => {
                await this.client.subscribe(user, mediaType);
                user.uid && (this.isHost(user.uid)) && await this._dispatchHostEnter();
                this.$emit('stream:started');
                (mediaType === 'video') && this._attachVideoOfParticipant(user.uid, user.videoTrack);
                (mediaType === 'audio') && this._attachAudioOfParticipant(user.uid, user.audioTrack);
            });
            this.client.on('user-left', async (user, reason) => {
                const uid = Number.parseInt(user.uid);
                if (uid === this.hostUid) {
                    if (this.hostUnpublishState === 0) {
                        this.hostUnpublishState = 1;
                        await this._dispatchKickLeave('host_left');
                        this.hostUnpublishState = 2;
                    }
                }
            });
            this.client.on('token-privilege-will-expire', async () => {
                // user could be kicked here, thus "freshToken && renew()"
                const freshToken = await this._fetchFreshToken();
                freshToken && await this.client.renewToken(freshToken);
            });
        },
        /*
         | Stream activation/deactivation methods
         */
        async startOrJoinStream () {
            // 1 - emit
            if (this.amIPublisher) {
                // start stream as publisher
                await axiosClient.post(`/api/v2/video-stream/start/${this.stream.channel}`);
                this.$set(this.stream, 'status', 'active');
                this.$root.$emit('live:started', this.stream.id);
            }

            // 2 - join
            // TODO 2022-02-03T20:19:40 any check required for `leave`?
            await this.leaveStream();
            await this.joinStream();
            this.haveIJoined = true;

            // 3 - publish tracks
            if (this.amIPublisher) {
                const myAudioTrack = await AgoraRTC.createMicrophoneAudioTrack();
                /**
                 * There is UID check in the method - because my own track shouldn't be played into my ears.
                 * Still we save a reference to it, for whatever.
                 */
                this._attachAudioOfParticipant(this.myRtcUid, myAudioTrack);
                //
                const myVideoTrack = await AgoraRTC.createCameraVideoTrack();
                this._attachVideoOfParticipant(this.myRtcUid, myVideoTrack);
                //
                await this.client.publish([myAudioTrack, myVideoTrack]);
                this.hasStreamBeenStartedRecently = true;
            }
            this.$loader.disable();
        },
        async joinStream (freshToken = undefined) {
            /**
             * User could enter the stream page, then leave immediately.
             * In that case streamManager is not created at all, hence check.
             * Multiple check because of async.
             */
            if (this.streamManager) {
                await this.streamManager.join(freshToken);
                if (this.streamManager) {
                    // TODO 2022-02-07T18:12:26 make for dev only
                    window.streamManager = this.streamManager;
                    this._addParticipant(this.streamManager.getUid(), this.streamManager.getRoleVerbose());
                }
            }
            this.haveIJoined = true;
        },
        async leaveStream () {
            /**
             * User could enter the stream page, then leave immediately.
             * In that case streamManager is not created at all, hence check.
             * Multiple check because of async.
             * ! this.streamManager is NOT unset here.
             */
            if (this.streamManager) {
                this.shutdownTracksOfUids(this.myRtcUid);
                await this.streamManager.leave();
                if (this.streamManager) {
                    this.$delete(this.participants, this.myRtcUid);
                }
            }
            this.haveIJoined = false;
        },
        async finishLiveStream (options = {}) {
            options = {
                redirect_to: null,
                ...options,
            };
            try {
                await axiosClient.post(`/api/v2/video-stream/end/${this.stream.id}`);
                this.$emit('live:finished', this.stream.id);
            } catch (e) {
                console.error(e);
            } finally {
                this._shutdownTracks();
                await this.leaveStream();
                this.hasStreamBeenFinishedRecently = true;
            }
        },
        _dispatchHostEnter () {
            if (!this.hasStreamBeenStartedRecently) {
                this.hasStreamBeenStartedRecently = true;
            }
        },
        /**
         * @param reason
         * ['host_left', 'token_expired']
         * @return {Promise<void>}
         * @private
         */
        async _dispatchKickLeave (reason) {
            this.$emit('stream:finished');
            this._shutdownTracks();
            this.hasStreamBeenFinishedRecently = true;
            await this.leaveStream();
            // alert('Stream was finished!');
            // could be overridden in components; ! do not declare onHostLeave() here
        },
        /**
         * TODO 2022-02-18T19:42:07 shutdown tracks (implement carefully)
         * @return {Promise<void>}
         */
        async leaveAndDestroyStream () {
            await this.leaveStream();
            this.streamManager && this.streamManager.destroy();
            this.participants = {};
            this.streamManager = undefined;
        },
        /*
         | Fetch
         */
        async _fetchFreshToken () {
            /**
             * '/authorize' endpoint uses api-auth-credentials (so to say, it is secure).
             * Thus when a publisher is watching himself as an audience-member he should receive a refreshed token
             * as an audience-member, that is why '/authorize-force-audience' is called.
             */
            const dataResponse = (this.amIPublisher)
                ? await axiosClient.get('/api/v2/video-stream/' + this.streamChannelName + '/authorize')
                : await axiosClient.get('/api/v2/video-stream/' + this.streamChannelName + '/authorize-force-audience')
            ;
            const data = dataResponse.data.data;
            if (data.do_kick_user) {
                this._dispatchKickLeave('token_expired');
                return;
            }
            return data.rtc_auth.token;
        },
        /*
         | Stream-related modifiers
         */
        _addPublishers (host, coHosts) {
            this._addParticipant(host, 'host');
            this._addParticipant(coHosts, 'co_host');
        },
        _addParticipant (uid, role) {
            const uids = (Array.isArray(uid) ? uid : [uid]).filter(v => !!v);
            for (uid of uids) {
                this.$set(this.participants, uid, {
                    uid,
                    role,
                    tracks: {},
                    is_muted: false,
                });
            }
        },
        /*
         | Calculators
         */
        filterParticipants (callback) {
            return Object.fromEntries(
                Object.entries(this.participants).filter(
                    ([k, v]) => callback(v, k)
                )
            );
        },
        getRoleByUid (uid) {
            return this.participants[uid]?.role;
        },
        isHost (uid) {
            return this.getRoleByUid(uid) === 'host';
        },
        isCoHost (uid) {
            return this.getRoleByUid(uid) === 'co_host';
        },
        /**
         * ! Reactive stream status is tested.
         * @return boolean
         */
        doesStreamHasStatus (haystack) {
            haystack = Array.isArray(haystack) ? haystack : [haystack];
            return haystack.includes(this.streamStatusReactive);
        },
        /**
         * TODO 2022-02-18T22:41:40 make safe int-converter helper, use it; implement
         * @param uid
         * @return {boolean|boolean}
         */
        isPartner (uid) {
            return false;
            // uid = Number.parseInt(uid);
            // it is not partner-user-id
            // return uid ? Number.parseInt(this.stream?.partner?.id) === uid : false;
        },
        /*
         | Tracks management
         */
        _attachVideoOfParticipant (uid, track) {
            if (!track) {
                return;
            }
            this._assignParticipantTrack(uid, 'video', track);
            if (this.isHost(uid)) {
                track.play('agora_remote', { fit: 'contain' });
            } else if (this.isCoHost(uid)) {
                track.play('agora_remote_cohost_' + uid, { fit: 'contain' });
            }
        },
        _attachAudioOfParticipant (uid, track, doForcePlay = false) {
            if (!track) {
                return;
            }
            this._assignParticipantTrack(uid, 'audio', track);
            if (doForcePlay || (uid !== this.myRtcUid)) {
                track.play();
            }
            /**
             * For audience: mute first host initially, another hosts has synced muted state.
             */
            if (!this.amIPublisher) {
                let flag = true;
                if (this.firstHostWithPublishedAudio === undefined) {
                    this.firstHostWithPublishedAudio = uid;
                } else {
                    flag = this.isMuted(this.firstHostWithPublishedAudio);
                }
                this.muteToggle(uid, flag);
            }
        },
        isMuted (uid) {
            return this.participants[uid]?.is_muted;
        },
        muteToggle (uid, flag = undefined) {
            if (this.participants[uid]) {
                const audioTrack = this.participants[uid].tracks?.audio;
                if (!audioTrack) {
                    return;
                }
                const shouldBeMuted = (flag === undefined) ? (!this.participants[uid]?.is_muted) : flag;
                if (shouldBeMuted) {
                    audioTrack.setVolume(0);
                    audioTrack.stop();
                    localStorage.removeItem(STORAGE_KEYS.IS_STREAM_MUTED);
                } else {
                    audioTrack.stop();
                    audioTrack.play();
                    audioTrack.setVolume(0);
                    audioTrack.setVolume(100);
                    localStorage.setItem(STORAGE_KEYS.IS_STREAM_MUTED, 'false');
                }
                this.$set(this.participants[uid], 'is_muted', shouldBeMuted);
            }
        },
        _assignParticipantTrack (uid, mediaType, track) {
            if (this.participants[uid]) {
                this.$set(this.participants[uid].tracks, mediaType, track);
            }
        },
        /**
         * TODO 2022-02-18T19:32:56 duplication with shutdownTracksByUid()
         */
        _shutdownTracks () {
            Object.keys(this.participants).forEach((uid) => {
                const tracks = this.participants[uid]?.tracks || {};
                Object.keys(tracks).forEach((mediaType) => {
                    tracks[mediaType]?.stop();
                    tracks[mediaType]?.close();
                });
                delete this.participants[uid].tracks;
                this.participants[uid].tracks = {};
            })
        },
        shutdownTracksOfUids (uid) {
            if (!uid) {
                return;
            }
            const uids = Array.isArray(uid) ? uid : [uid];
            for (uid of uids) {
                const tracks = this.participants[uid]?.tracks || {};
                Object.keys(tracks).forEach((mediaType) => {
                    tracks[mediaType]?.stop();
                    tracks[mediaType]?.close();
                });
            }
        },
    },
}

export default useStreamClientMixin;
