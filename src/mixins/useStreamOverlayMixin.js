import {mapGetters, mapMutations} from 'vuex';
import MODAL_STATES from '../contants/MODAL_STATES';

export default {
    computed: {
        ...mapGetters({
            'currentModalState': 'CURRENT_MODAL_STATE',
        }),
    },
    methods: {
        ...mapMutations({
            '$setModalState': 'SET_MODAL_STATE'
        }),
        setFullScreen () {
            if (this.currentModalState === MODAL_STATES.DEFAULT) {
                this.$setModalState(MODAL_STATES.FULLSCREEN);
            }
            if (this.currentModalState === MODAL_STATES.MINIMIZED) {
                this.$setModalState(MODAL_STATES.DEFAULT);
            }
        },
        setDefault () {
            this.$setModalState(MODAL_STATES.DEFAULT);
        },
        setMinimized () {
            this.$setModalState(MODAL_STATES.MINIMIZED);
        },
        disableFullScreen () {
           /* let prevModalState = this.$store.getters.PREV_MODAL_STATE;
            if (!prevModalState) {
                prevModalState = MODAL_STATES.DEFAULT;
            }*/
            this.$setModalState(MODAL_STATES.DEFAULT);
        },
    }
}
