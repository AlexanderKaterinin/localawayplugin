import axiosClient from '../utils/axiosClient';
import STORAGE_KEYS from '../contants/STORAGE_KEYS';

export default {
    state: {
        messages: [],
        chatName: localStorage.getItem(STORAGE_KEYS.CHAT_NAME),
        showLoginModal: false,
    },
    getters: {
        messages: s => s.messages.slice().reverse(),
        messagesByChannel: s => channel => s.messages.filter(m => m.channel === channel && m.message_text).slice().reverse(),
        chatName: s => s.chatName,
        showLoginModal: s => s.showLoginModal,
    },
    mutations: {
        setMessages (state, messages) {
            state.messages = messages;
        },
        addMessage (state, message) {
            const messageIndex = state.messages.findIndex(i => i.id === message.id);
            if (messageIndex !== -1) {
                state.messages.splice(messageIndex, 1, message)
            } else {
                state.messages.push(message);
            }
        },
        deleteMessage (state, id) {
            state.messages = state.messages.filter(m => m.id !== id);
        },
        setChatName (state, name) {
            localStorage.setItem(STORAGE_KEYS.CHAT_NAME, name);
            state.chatName = name;
        },
        setShowLoginModal (state, bool) {
            state.showLoginModal = bool;
        }
    },
    actions: {
        async sendMessage ({ commit }, payload) {
            const { data: { data } } = await axiosClient.post(`/api/v2/chat/message`, payload);
            commit('addMessage', data);
        },
        async getMessages ({ commit }, channel) {
            const { data: { data } } = await axiosClient.get(`/api/v2/chat/${channel}`);
            commit('setMessages', data.messages);
        }
    }
}
