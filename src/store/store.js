import Vue from 'vue';
import Vuex from 'vuex';
import core from './core';
import events from './events';
import chat from './chat';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        core, events, chat
    }
});

export default store;
