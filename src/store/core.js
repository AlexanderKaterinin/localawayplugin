import axiosClient from '../utils/axiosClient';
import MODAL_STATES from '../contants/MODAL_STATES';

export default {
    namespace: 'core',
    state: {
        API_KEY: undefined,
        API_HOST: undefined,
        AGORA_APP_ID: undefined,
        IS_APP_READY: false,
        CURRENT_MODAL_STATE: null,
        PREV_MODAL_STATE: undefined,
        toast_text: '',
        toast_type: '',
        PLUGIN_POSITION: 'right',
    },
    getters: {
        AGORA_APP_ID: state => state.AGORA_APP_ID,
        IS_APP_READY: state => state.IS_APP_READY,
        CURRENT_MODAL_STATE: s => s.CURRENT_MODAL_STATE,
        PREV_MODAL_STATE: s => s.PREV_MODAL_STATE,
        PLUGIN_POSITION: s => s.PLUGIN_POSITION,
    },
    mutations: {
        SET_DEFAULTS(state, { API_KEY, API_HOST }) {
            state.API_KEY = API_KEY;
            state.API_HOST = API_HOST
        },
        SET_APP_ID (state, payload) {
            state.AGORA_APP_ID = payload;
        },
        SET_IS_APP_READY (state, payload) {
            state.IS_APP_READY = payload;
        },
        SET_MODAL_STATE (state, payload) {
            state.PREV_MODAL_STATE = state.CURRENT_MODAL_STATE;
            state.CURRENT_MODAL_STATE = payload;
        },
        showToast (state, { text, type }) {
            state.toast_text = text;
            state.toast_type = type;
        },
        setPluginPosition (state, position) {
            state.PLUGIN_POSITION = position;
        }
    },
    actions: {
        async GET_INITIAL ({ commit, state }) {
            try {
                const { data: { data } } = await axiosClient.get(`/api/v2/promoter/${state.API_KEY}`);
                let stylists = [];
                if (data.stylists.length) {
                    stylists.push(data.stylists.at(-1))
                }
                commit('setPluginPosition', data.meta.plugin_position);
                commit('SET_STYLISTS', stylists);
                commit('SET_APP_ID', data.AGORA_APP_ID);
                commit('SET_IS_APP_READY', true);
            } catch (e) {
                throw e;
            }
        },
    }
}
