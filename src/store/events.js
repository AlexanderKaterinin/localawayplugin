import STORAGE_KEYS from '../contants/STORAGE_KEYS';
import MODAL_STATES from '../contants/MODAL_STATES';

export default {
    state: {
        STYLISTS: null,
        CHOSEN_STYLIST: null,
        ACTIVE_EVENT: null,
        ACTIVE_EVENT_TYPE: null,
        STREAM_STATS: null,
    },
    getters: {
        STYLISTS: state => state.STYLISTS,
        CHOSEN_STYLIST: s => s.CHOSEN_STYLIST,
        ACTIVE_EVENT: s => s.ACTIVE_EVENT,
        ACTIVE_EVENT_TYPE: s => s.ACTIVE_EVENT_TYPE,
        VIEWERS_COUNT: s => s.STREAM_STATS?.audience_count,
        PICKED_PRODUCT_ID: s => {
            let products = s.STREAM_STATS?.custom_reactive_data?.products_attrs;
            if (!products) {
                return null;
            }
            let productId = null;
            Object.keys(products).forEach((index) => {
                if (products[index].is_picked === true) {
                    productId = index;
                }
            })
            return productId;
        }
    },
    mutations: {
        STREAM_WAS_STARTED (state, payload) {
            state.STYLISTS = state.STYLISTS.map(s => {
               if (s.id === payload.user_id) {
                   s.is_online = true;
                   s.events = s.events.map(event => {
                       if (payload.id === event.id) {
                           event.status = 'active';
                       }
                       return event;
                   })
               }
               return s;
            });

            if (state.ACTIVE_EVENT?.id === payload.id && state.ACTIVE_EVENT_TYPE === 'stream') {
                state.ACTIVE_EVENT = {
                    ...state.ACTIVE_EVENT,
                    status: 'active',
                }
            }
        },
        SET_STYLISTS (state, payload) {
            state.STYLISTS = payload;
            state.IS_APP_READY = true;
        },
        SET_CHOSEN_STYLIST (state, payload) {
            localStorage.setItem(STORAGE_KEYS.ACTIVE_STYLIST_ID, payload.id);
            state.CHOSEN_STYLIST = payload;
        },
        SET_ACTIVE_EVENT (state, payload) {
            localStorage.setItem(STORAGE_KEYS.ACTIVE_EVENT_ID, payload.id);
            state.STREAM_STATS = null;
            state.ACTIVE_EVENT_TYPE = payload.event_type;
            state.ACTIVE_EVENT = payload;
        },
        SET_STREAM_FINISHED (state, payload) {
            state.ACTIVE_EVENT = {
                ...payload,
                status: 'finished'
            };
            state.STYLISTS = state.STYLISTS.map(stylist => {
                if (stylist.id == payload.user_id) {
                    stylist.is_online = false;
                    stylist.events = stylist.events.map(event => {
                        if (event.id == payload.id && event.event_type == payload.event_type) {
                            event = {...payload, status: 'finished'};
                        }
                        return event;
                    })
                }
                return stylist;
            })
        },
        SET_ACTIVE_EVENT_TYPE (state, type) {
            state.ACTIVE_EVENT_TYPE = type;
        },
        CLEAR_CHOSEN (state) {
            localStorage.removeItem(STORAGE_KEYS.ACTIVE_EVENT_ID);
            localStorage.removeItem(STORAGE_KEYS.ACTIVE_STYLIST_ID);
            state.CHOSEN_STYLIST = null;
            state.ACTIVE_EVENT_TYPE = null;
            state.ACTIVE_EVENT = null;
            state.STREAM_STATS = null;
        },
        SET_STREAM_STATS (state, stats) {
            state.STREAM_STATS = stats;
        }
    },
    actions: {
        async CHOOSE_STYLIST ({ commit, dispatch, getters }, stylist) {
            if (stylist === null) {
                commit('CLEAR_CHOSEN');
                localStorage.removeItem(STORAGE_KEYS.IS_STREAM_MUTED);
                return null;
            }

            if (getters.CHOSEN_STYLIST !== null) {
                commit('CLEAR_CHOSEN');
                dispatch('CHOOSE_STYLIST', stylist);
            }

            if (getters.CURRENT_MODAL_STATE === null) {
                commit('SET_MODAL_STATE', MODAL_STATES.DEFAULT);
            }

            if (!stylist.events.length) {
                alert('No active events for chosen stylist!');
                return false;
            }
            if (!getters.ACTIVE_EVENT) {
                const foundEvent = stylist.events.find(e => e.id === stylist?.needleEventId);
                if (foundEvent) {
                    dispatch('CHOOSE_EVENT', foundEvent);
                } else {
                    const activeStream = stylist.events.find(e => e.event_type === 'stream' && e.status === 'active');
                    const activeEvent = activeStream ? activeStream : stylist.events[0];
                    dispatch('CHOOSE_EVENT', activeEvent);
                }
            }
            commit('SET_CHOSEN_STYLIST', stylist);
        },
        CHOOSE_EVENT ({ commit }, event) {
            commit('SET_ACTIVE_EVENT', event);
            commit('SET_ACTIVE_EVENT_TYPE', event.event_type);
        },
        RESTORE_PREVIOUS_STATE ({ commit, getters, dispatch }) {
            let isLinkWasShared = false;
            const currentURL = new URL(window.location.href);
            const searchParams = currentURL.searchParams;
            let eventParam = searchParams.get('liveshopping');
            if (eventParam) {
                try {
                    const decodedString = JSON.parse(atob(eventParam));
                    const stylist = getters.STYLISTS.find(s => s.id === +decodedString.stylist_id);
                    if (stylist) {
                        const event = stylist.events.find(e => e.id === +decodedString.event_id);
                        if (event) {
                            commit('SET_MODAL_STATE', MODAL_STATES.DEFAULT);
                            dispatch('CHOOSE_EVENT', event);
                            dispatch('CHOOSE_STYLIST', stylist);
                            isLinkWasShared = true;
                        }
                    }
                } catch (e) {
                    //
                }
            }

            if (isLinkWasShared) {
                return ;
            }

            const prevStylistId = localStorage.getItem(STORAGE_KEYS.ACTIVE_STYLIST_ID);
            const prevEventId = localStorage.getItem(STORAGE_KEYS.ACTIVE_EVENT_ID);
            if (!(prevStylistId && prevEventId)) {
                commit('CLEAR_CHOSEN');
                return ;
            }
            const stylist = getters.STYLISTS.find(s => s.id === +prevStylistId);
            if (!stylist) {
                commit('CLEAR_CHOSEN');
                return ;
            }
            const event = stylist.events.find(e => e.id === +prevEventId);
            if (!event || event.event_type === 'day') {
                commit('CLEAR_CHOSEN');
                return ;
            }

            if (event?.status === 'planned') {
                commit('SET_MODAL_STATE', MODAL_STATES.DEFAULT);
            } else {
                commit('SET_MODAL_STATE', MODAL_STATES.MINIMIZED);
            }
            dispatch('CHOOSE_EVENT', event);
            dispatch('CHOOSE_STYLIST', stylist);
        }
    }
}
