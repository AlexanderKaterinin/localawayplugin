const { createVuePlugin } = require('vite-plugin-vue2');

module.exports = {
    build: {
        rollupOptions: {
            output: [
                {
                    format: 'iife'
                }
            ]
        }
    },
    plugins: [createVuePlugin()],
};
